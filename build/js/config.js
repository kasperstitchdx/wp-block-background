import {__} from '@wordpress/i18n';

// Editable
export const namespace = 'narwhal';
export const blockName = 'background';
export const blockTitle = __('Background', 'narwhal');

// Auto-Generated
export const blockType = `${namespace}/${blockName}`;
export const baseClassName = `wp-block-${namespace}-${blockName}`;

// Helpers
export const validAlignments = ['wide', 'full'];

export function getEditWrapperProps(attributes) {
    const {align} = attributes;
    if (-1 !== validAlignments.indexOf(align)) {
        return {'data-align': align};
    }
}
