import {registerBlockType} from '@wordpress/blocks';
import {applyFilters} from '@wordpress/hooks';
import {__} from '@wordpress/i18n';
import {blockTitle, blockType, getEditWrapperProps} from './config';
import edit from './edit';
import save from './save';

registerBlockType(blockType, {
    title: blockTitle,
    icon: 'admin-customizer',
    category: 'layout',
    keywords: [__('image', 'narwhal'), __('color', 'narwhal')],
    attributes: {
        align: {
            default: applyFilters('narwhal.blocks.background.defaultAlignment', 'full'),
        },
        backgroundColor: {
            default: applyFilters('narwhal.blocks.background.defaultBackgroundColor', ''),
        },
        customBackgroundColor: {},
        customTextColor: {},
        hasOverlay: {
            default: false,
            type: 'boolean',
        },
        imageAlt: {},
        imageId: {
            type: 'number',
        },
        imageSrc: {},
        overlayOpacity: {
            default: applyFilters('narwhal.blocks.background.defaultOverlayOpacity', 50),
            type: 'number',
        },
        overlayStyle: {
            default: applyFilters('narwhal.blocks.background.defaultOverlayStyle', ''),
        },
        textColor: {
            default: applyFilters('narwhal.blocks.background.defaultTextColor', ''),
        },
    },
    edit,
    save,
    supports: {
        anchor: true,
        html: false,
    },
    getEditWrapperProps,
});
