import {getColorClassName, InnerBlocks} from '@wordpress/editor';
import classNames from 'classnames';

import {baseClassName} from './config';

function Save({attributes, className}) {

    const {
        align,
        backgroundColor,
        customBackgroundColor,
        customTextColor,
        hasOverlay,
        imageAlt,
        imageId,
        imageSrc,
        overlayOpacity,
        overlayStyle,
        textColor,
    } = attributes;

    const backgroundClass = getColorClassName('background-color', backgroundColor);
    const textClass = getColorClassName('color', textColor);

    const styles = {
        backgroundColor: backgroundClass ? undefined : customBackgroundColor,
        color: textClass ? undefined : customTextColor,
    };

    const renderBackgroundImage = () => {
        const overlayStyles = {
            opacity: Number.isInteger(overlayOpacity) ? overlayOpacity / 100 : undefined,
        };
        const overlayClasses = classNames(`${baseClassName}__image-overlay`, {
            'has-overlay': hasOverlay && overlayStyle,
            [`has-${overlayStyle}-overlay`]: hasOverlay && overlayStyle,
        });

        return (
            <div className={`${baseClassName}__background-underlay`}>
                <img
                    alt={imageAlt}
                    className={classNames(`${baseClassName}__image`, {
                        [`wp-image-${imageId}`]: imageId,
                    })}
                    src={imageSrc}
                />
                {hasOverlay ? (
                    <div className={overlayClasses} style={overlayStyles} />
                ) : null}
            </div>
        );
    };

    return (
        <div
            className={classNames(className, {
                [`align${align}`]: align,
                'has-background': backgroundColor || customBackgroundColor,
                [backgroundClass]: backgroundClass,
                'has-text-color': textColor || customTextColor,
                [textClass]: textClass,
            })}
            style={styles}
        >
            <div className={`${baseClassName}__inner`}>
                <InnerBlocks.Content />
            </div>
            {imageSrc ? renderBackgroundImage() : null}
        </div>
    );

}

export default Save;
