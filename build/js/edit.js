import {__} from '@wordpress/i18n';
import {Component, Fragment} from '@wordpress/element';
import {compose} from '@wordpress/compose';
import {PanelBody, RangeControl, SelectControl, ToggleControl, withFallbackStyles} from '@wordpress/components';
import {
    BlockAlignmentToolbar,
    BlockControls,
    ContrastChecker,
    InnerBlocks,
    InspectorControls,
    PanelColorSettings,
    withColors,
} from '@wordpress/editor';
import {applyFilters} from '@wordpress/hooks';
import {MediaField} from '@narwhal/components';
import classNames from 'classnames';
import {baseClassName, validAlignments} from './config';
import _ from 'lodash';

const {getComputedStyle} = window;

const applyFallbackStyles = withFallbackStyles((node, ownProps) => {
    const {backgroundColor, textColor} = ownProps;
    const backgroundColorValue = backgroundColor && backgroundColor.color;
    const textColorValue = textColor && textColor.color;
    return {
        fallbackBackgroundColor: backgroundColorValue || !node ? undefined : getComputedStyle(node).backgroundColor,
        fallbackTextColor: textColorValue || !node ? undefined : getComputedStyle(node).color,
    };
});

const imageSize = applyFilters('narwhal.blocks.background.imageSize', 'large');

const overlayOptions = applyFilters('narwhal.blocks.background.overlayOptions', []);

class BackgroundEdit extends Component {
    constructor() {
        super(...arguments);
        this.nodeRef = null;
        this.bindRef = this.bindRef.bind(this);
        this.removeImage = this.removeImage.bind(this);
        this.selectImage = this.selectImage.bind(this);
    }

    componentDidMount() {
        if (Array.isArray(overlayOptions) && 1 === overlayOptions.length && overlayOptions[0].value) {
            this.props.setAttributes({overlayStyle: overlayOptions[0].value});
        }
    }

    bindRef(node) {
        if (!node) {
            return;
        }
        this.nodeRef = node;
    }

    removeImage() {
        this.props.setAttributes({
            imageAlt: '',
            imageId: 0,
            imageSrc: '',
        });
    }

    selectImage(image) {
        this.props.setAttributes({
            imageAlt: image.alt,
            imageId: image.id,
            imageSrc: _.get(image, ['sizes', imageSize, 'url'], image.url),
        });
    }

    render() {
        const {
            attributes,
            backgroundColor,
            className,
            insertBlocksAfter,
            fallbackBackgroundColor,
            fallbackTextColor,
            isSelected,
            setAttributes,
            setBackgroundColor,
            setTextColor,
            textColor
        } = this.props;

        const {
            align,
            imageAlt,
            imageId,
            imageSrc,
        } = attributes;

        const updateAlignment = (align) => setAttributes({align});

        return (
            <Fragment>
                <div
                    className={classNames(className, {
                        'has-background': backgroundColor.color,
                        [backgroundColor.class]: backgroundColor.class,
                        'has-text-color': textColor.color,
                        [textColor.class]: textColor.class,
                    })}
                    style={{
                        backgroundColor: backgroundColor.color,
                        color: textColor.color,
                    }}
                >
                    <div className={`${className}__inner`}>
                        {/* The following conditional is a workaround for https://github.com/WordPress/gutenberg/issues/9897 */}
                        {'undefined' !== typeof insertBlocksAfter ? <InnerBlocks templateLock={false} /> : null}
                    </div>
                    {imageSrc ? this.renderBackgroundImage() : null}
                </div>
                <BlockControls>
                    <BlockAlignmentToolbar
                        value={align}
                        controls={validAlignments}
                        onChange={updateAlignment}
                    />
                </BlockControls>
                <InspectorControls>
                    <PanelColorSettings
                        title={__('Color Settings', 'narwhal')}
                        initialOpen={false}
                        colorSettings={[
                            {
                                value: backgroundColor.color,
                                onChange: setBackgroundColor,
                                label: __('Background Color', 'narwhal'),
                            },
                            {
                                value: textColor.color,
                                onChange: setTextColor,
                                label: __('Text Color', 'narwhal'),
                            },
                        ]}
                    >
                        <ContrastChecker {...{
                            // Text is considered large if font size is greater or equal to 18pt or 24px,
                            // currently that's not the case for button.
                            isLargeText: false,
                            textColor: textColor.color,
                            backgroundColor: backgroundColor.color,
                            fallbackBackgroundColor,
                            fallbackTextColor,
                        }} />
                    </PanelColorSettings>
                    <PanelBody
                        title={__('Image Settings', 'narwhal')}
                        initialOpen={false}
                    >
                        <MediaField
                            isSelected={isSelected}
                            labels={{
                                title: __('Background Image (optional)', 'narwhal'),
                                name: __('image', 'narwhal'),
                            }}
                            onRemove={this.removeImage}
                            onSelect={this.selectImage}
                            previewRender={() => (
                                <div className="components-base-control">
                                    <img src={imageSrc} alt={imageAlt} />
                                </div>
                            )}
                            value={imageId}
                        />

                        {imageId ? this.renderOverlayOptions() : null}
                    </PanelBody>
                </InspectorControls>
            </Fragment>
        );
    }

    renderBackgroundImage() {
        const {
            imageAlt,
            imageId,
            imageSrc,
            hasOverlay,
            overlayOpacity,
            overlayStyle,
        } = this.props.attributes;
        const overlayStyles = {
            opacity: Number.isInteger(overlayOpacity) ? overlayOpacity / 100 : undefined,
        };
        const overlayClasses = classNames(`${baseClassName}__image-overlay`, {
            'has-overlay': hasOverlay && overlayStyle,
            [`has-${overlayStyle}-overlay`]: hasOverlay && overlayStyle,
        });

        return (
            <div className={`${baseClassName}__background-underlay`}>
                <img
                    alt={imageAlt}
                    className={classNames(`${baseClassName}__image`, {
                        [`wp-image-${imageId}`]: imageId,
                    })}
                    src={imageSrc}
                />
                {hasOverlay ? (
                    <div className={overlayClasses} style={overlayStyles} />
                ) : null}
            </div>
        );
    }

    renderOverlayOptions() {
        if (!Array.isArray(overlayOptions) || 1 > overlayOptions.length) {
            return null;
        }

        const {
            setAttributes,
        } = this.props;
        const {
            hasOverlay,
            overlayOpacity,
            overlayStyle,
        } = this.props.attributes;
        return (
            <Fragment>
                <ToggleControl
                    checked={hasOverlay}
                    label={__('Use image overlay', 'narwhal')}
                    onChange={() => {
                        setAttributes({hasOverlay: !hasOverlay})
                    }}
                />
                {hasOverlay && 1 < overlayOptions.length ? (
                    <SelectControl
                        label={__('Overlay Style', 'narwhal')}
                        onChange={overlayStyle => setAttributes({overlayStyle})}
                        options={overlayOptions}
                        value={overlayStyle}
                    />
                ) : null}
                {hasOverlay && overlayStyle ? (
                    <RangeControl
                        label={__('Overlay opacity (%)', 'narwhal')}
                        max="100"
                        min="1"
                        onChange={overlayOpacity => setAttributes({overlayOpacity})}
                        value={overlayOpacity}
                    />
                ) : null}
            </Fragment>
        );
    }
}

export default compose([
    withColors('backgroundColor', {textColor: 'color'}),
    applyFallbackStyles,
])(BackgroundEdit);
