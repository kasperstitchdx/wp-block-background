# Background Block

## Filters
`narwhal.blocks.background.defaultAlignment`
* Default value: `full`
* Defines the default block alignment value.

`narwhal.blocks.background.defaultBackgroundColor`
* Default value: empty string
* Defines the default background color to use for new blocks.

`narwhal.blocks.background.defaultOverlayOpacity`
* Default value: `50` (int)
* Defines the default overlay opacity. Can be any integer from 1 to 100.

`narwhal.blocks.background.defaultOverlayStyle`
* Default value: empty string
* Defines the default background image overlay style to use.
* NOTE: No overlay styles are defined by default. You will need to use the `` filter to set overlay styles.

`narwhal.blocks.background.defaultTextColor`
* Default value: empty string
* Defines the default text color to use for new blocks.

`narwhal.blocks.background.imageSize`
* Default value: `large`
* Defines the image size to use for background images. Can be any WordPress named image size.
* NOTE: Image size will need to be filtered to be allowed to show in the admin as of WP 5.0. If it doesn't appear as an option in the normal image inserter, it cannot be used here.

`narwhal.blocks.background.overlayOptions`
* Default value: empty array
* Defines the overlay options available to the user.
* Should be an array of objects, each containing a `label` and a `value` property.
* Values will be placed into the following classname format for styling: `has-{value}-overlay`.
