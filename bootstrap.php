<?php

namespace Stitchdx\WordPress\Blocks\Background;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

// Add actions.
add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\\registerBlockEditorAssets', 5 );
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\enqueueBlockFrontendScripts', 5 );
add_filter( 'safe_style_css', __NAMESPACE__ . '\\allowOpacityStyle' );

/**
 * Returns the block version number.
 *
 * @return string
 */
function version() {
	return '1.0';
}

/**
 * Returns the name of the block.
 *
 * @return string
 */
function blockName() {
	return 'background';
}

/**
 * Enqueues styles and scripts for the admin editor.
 */
function registerBlockEditorAssets() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	$fileName = blockName();

	wp_enqueue_style( "narwhal-block-{$fileName}-editor", assetUrl( "assets/css/{$fileName}-editor{$suffix}.css" ), [], version() );

	wp_enqueue_script( "narwhal-block-{$fileName}-editor", assetUrl( "assets/js/{$fileName}-editor{$suffix}.js" ), [
		'lodash',
		'narwhal-block-components',
		'wp-blocks',
		'wp-components',
		'wp-compose',
		'wp-editor',
		'wp-element',
		'wp-hooks',
		'wp-i18n',
	], version(), true );
}

/**
 * Enqueues styles and scripts for the frontend.
 */
function enqueueBlockFrontendScripts() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	$fileName = blockName();

	wp_enqueue_style( "narwhal-block-{$fileName}", assetUrl( "assets/css/{$fileName}{$suffix}.css" ), [], version() );
}

/**
 * Returns appropriate URL for block assets.
 *
 * @param $path
 *
 * @return mixed
 */
function assetUrl( $path ) {
	if ( defined( 'NARWHAL_BLOCKS_URL' ) ) {
		/**
		 * If NARWHAL_BLOCKS_URL is defined, use it. Should be full URL to the directory containing all the Narwhal
		 * blocks. In a composer setup, it would be the `narwhal-digital` namespace folder.
		 */
		return rtrim( NARWHAL_BLOCKS_URL, '/' ) . '/' . basename( __DIR__ ) . '/' . ltrim( $path, '/' );
	}

	return plugins_url( $path, __FILE__ );
}

/**
 * Adds `opacity` to the list of safe styles. This allows the background block to have colored image overlays with
 * customizable transparency.
 *
 * @param array $styles
 *
 * @return array
 */
function allowOpacityStyle( array $styles ) {
	$styles[] = 'opacity';

	return $styles;
}