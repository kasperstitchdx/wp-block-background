'use strict';

const blockName = 'background';

const autoprefixer = require('autoprefixer');
const browsers = require('@wordpress/browserslist-config');
const fs = require('fs');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = function () {
    const mode = process.env.NODE_ENV || 'development';
    const isProduction = mode === 'production';

    const externals = {
        '@wordpress/blocks': 'wp.blocks',
        '@wordpress/components': 'wp.components',
        '@wordpress/compose': 'wp.compose',
        '@wordpress/data': 'wp.data',
        '@wordpress/editor': 'wp.editor',
        '@wordpress/element': 'wp.element',
        '@wordpress/hooks': 'wp.hooks',
        '@wordpress/i18n': 'wp.i18n',
        '@narwhal/components': 'narwhalWp.components',
        jquery: 'jQuery',
        lodash: 'lodash',
    };

    const loaders = {
        css: {
            loader: 'css-loader',
            options: {
                sourceMap: true
            }
        },
        postCss: {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    autoprefixer({
                        browsers,
                        flexbox: 'no-2009'
                    })
                ],
                sourceMap: true
            }
        },
        sass: {
            loader: 'sass-loader',
            options: {
                sourceMap: true,
                includePaths: [path.resolve('./node_modules')],
            }
        }
    };

    const extensionPrefix = isProduction ? '.min' : '';

    // Relative path.
    const basePath = './';

    const paths = {
        css: `${basePath}/assets/css/`,
        img: `${basePath}/assets/img/`,
        js: `${basePath}/assets/js/`,
        lang: `${basePath}/languages/`,
    };

    // Entry points. Key will be used as output file name.
    const entry = {
        [blockName]: [
            `${basePath}/build/scss/style.scss`,
        ],
        [`${blockName}-editor`]: [
            `${basePath}/build/js/index.js`,
            `${basePath}/build/scss/editor.scss`,
        ],
    };

    const config = {
        mode,
        entry,
        optimization: {
            minimizer: isProduction ? [
                new TerserPlugin(),
                new OptimizeCssAssetsPlugin({}),
            ] : [],
        },
        output: {
            path: path.join(__dirname, '/'),
            filename: `${paths.js}[name]${extensionPrefix}.js`,
        },
        externals,
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js|.jsx/,
                    loader: 'import-glob',
                    exclude: /node_modules/,
                },
                {
                    test: /\.js|.jsx/,
                    loader: 'babel-loader',
                    query: {
                        presets: [
                            '@wordpress/default',
                        ],
                        plugins: [
                            '@wordpress/babel-plugin-import-jsx-pragma',
                            '@babel/transform-react-jsx',
                            [
                                '@wordpress/babel-plugin-makepot',
                                {
                                    'output': `${ paths.lang }translation.pot`,
                                }
                            ],
                            'transform-class-properties',
                        ],
                    },
                    exclude: /node_modules/
                },
                {
                    test: /\.html$/,
                    loader: 'raw-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, loaders.css, loaders.postCss],
                    exclude: /node_modules/
                },
                {
                    test: /\.scss$/,
                    use: [MiniCssExtractPlugin.loader, loaders.css, loaders.postCss, loaders.sass],
                    exclude: /node_modules/
                },
                {
                    test: /\.(gif|jpg|png)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: paths.img,
                                publicPath: '../img',
                            }
                        }
                    ],
                },
                {
                    test: /\.(svg)?$/,
                    use: [
                        {
                            loader: 'svg-url-loader',
                            options: {
                                stripdeclarations: true,
                                iesafe: true,
                                encoding: 'none',
                                limit: 2048,
                                name: '[name].[ext]',
                                outputPath: paths.img,
                                publicPath: '../img',
                            }
                        }
                    ],
                }
            ],
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `${paths.css}[name]${extensionPrefix}.css`
            }),
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(mode),
            }),
            // Custom webpack plugin - remove generated JS files that aren't needed
            function () {
                this.hooks.done.tap('webpack', function (stats) {
                    stats.compilation.chunks.forEach(chunk => {
                        if (!chunk.entryModule._identifier.includes(".js")) {
                            chunk.files.forEach(file => {
                                if (fs.existsSync(file) && file.includes(".js")) {
                                    fs.unlinkSync(path.join(__dirname, `/${file}`));
                                }
                            });
                        }
                    });
                });
            },
        ],
    };

    if (!isProduction) {
        config.devtool = 'source-map';
    }

    return config;
};
